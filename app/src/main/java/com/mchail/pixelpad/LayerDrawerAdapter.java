package com.mchail.pixelpad;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by mchail on 6/13/15.
 */
public class LayerDrawerAdapter extends RecyclerView.Adapter<LayerDrawerAdapter.LayerItemViewHolder>{

    ArrayList<Layer> layers;

	private int selected = 0;
	
	private static final int TYPE_NORMAL = 0;
	private static final int TYPE_GHOST = -1;
	
	
    OnLayerSelectListener mLayerListener;


    public LayerDrawerAdapter(ArrayList<Layer> layers) {
        this.layers = layers;
    }

	public void setSelected(int selected) {
		int o= this.selected ;
		this.selected = selected;
		
		notifyItemChanged(o);
		notifyItemChanged(selected);
		
	}

	@Override
	public int getItemViewType(int position) {
		return super.getItemViewType(position);
	}

	

    @Override
    public LayerItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View layerView = inflater.inflate(R.layout.layer, parent, false);
        return new LayerItemViewHolder(layerView);
    }

    @Override
    public void onBindViewHolder(LayerItemViewHolder holder, int position) {
        holder.layerThumbnail.setImageBitmap(layers.get(position).getBitmap());
        holder.itemView.setTag(position);
		if(position == selected){
			holder.itemView.setBackgroundResource(R.drawable.layer_item_selected_bg);
		}else{
			holder.itemView.setBackgroundResource(R.drawable.layer_item_bg);
		}
    }

    @Override
    public int getItemCount() {
        return layers.size();
    }

    public final class LayerItemViewHolder extends RecyclerView.ViewHolder{

        public ImageView layerThumbnail;

        public LayerItemViewHolder(View itemView) {
            super(itemView);
            layerThumbnail = (ImageView)itemView.findViewById(R.id.thumbnail);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mLayerListener != null){
						int pos = (Integer)v.getTag();
                        mLayerListener.onLayerSelect(pos);
						setSelected(pos);
						
                    }
                }
            });
        }
    }

    public interface OnLayerSelectListener{
        void onLayerSelect(int layer);
    }


    public void setOnLayerSelectListener(OnLayerSelectListener mLayerListener) {
        this.mLayerListener = mLayerListener;
    }
}
