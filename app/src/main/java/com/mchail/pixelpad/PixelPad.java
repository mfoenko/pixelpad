package com.mchail.pixelpad;

import android.graphics.*;
import com.mchail.pixelpad.tools.*;
import java.util.*;

public class PixelPad 
{
	
	
	public Point canvasSize;
	public ArrayList<Layer> layers = new ArrayList<Layer>();
	public ArrayList<HistoryEntry> history = new ArrayList<HistoryEntry>();
	public int curentLayerIndex=0;
	public Tool tool;

	public static final int MAX_HISTORY = 5;
	
	public PixelPad(Point mCanvasSize) {
		this.canvasSize = mCanvasSize;
		addLayer();
	}
	
	public PixelPad(int width, int height){
		this(new Point(width,height));
	}
	
	
	public int addLayer(){
		layers.add(new Layer(canvasSize));
		return layers.size();
	}
	
	public void addToHistory(){
		HistoryEntry e;
		
		if(history.size() >= MAX_HISTORY){
			e = history.remove(history.size()-1);
			e.setLayer(layers.get(curentLayerIndex));
		}else{
			e = new HistoryEntry(layers.get(curentLayerIndex));
		}
		
		history.add(0, e);
	}
	
	public void recallHistory(){
		if(history.size() < 1) return;
		history.remove(0).recall();
		
	}
	
	public void setSize(int width, int height) {
		canvasSize.x = width;
		canvasSize.y = height;
		layers.add(new Layer(width, height));
	}


	public void setTool(Tool tool) {
		this.tool = tool;
	}

	public boolean begin() {
		if (tool == null) {
			return false;
		}

		//Layer toolLayer = new Layer(layers.get(curentLayerIndex));

		return tool.begin(layers.get(curentLayerIndex));
	}

	public void finish() {
		if (tool == null) return;

		tool.finish();
		tool = null;
	}


	public Bitmap getImage() {
		int[][] pixels = Layer.merge(layers.toArray(new Layer[0])).pixels;
		int[] buffer = new int[canvasSize.x*canvasSize.y];
		for(int x=0;x<canvasSize.x ;x++){
			for(int y=0;y<canvasSize.y;y++){
				buffer[x*canvasSize.y+y] = pixels[y][x];
			}
		}

		Bitmap bmp = Bitmap.createBitmap(buffer,canvasSize.x, canvasSize.y, Bitmap.Config.ARGB_4444);
		//bmp.copyPixelsFromBuffer(IntBuffer.wrap(buffer));
		//Bitmap bmp = Bitmap.createBitmap(buffer, 
		return bmp;
	}
	
	
	
}
