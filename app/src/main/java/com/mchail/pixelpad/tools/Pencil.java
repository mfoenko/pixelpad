package com.mchail.pixelpad.tools;
import android.graphics.*;
import android.util.*;
import android.view.*;
import android.view.View.*;
import com.mchail.pixelpad.*;
import com.mchail.pixelpad.views.*;
import android.widget.*;


public class Pencil extends Tool {


	int mColor = 0xFF000000;
	float mRadius = 0;

	View optionsView;

	public void setColor(int argb) {
		mColor = argb;
	}
	public void setColor(int a, int r, int g, int b) {
		mColor = Color.argb(a, r, g, b);
	}



	@Override
	public Layer draw(int x, int y, boolean isRighyX, boolean isBottomY) {
		//Log.i("AAA", x + " " + y + " " + isRighyX + " " + isBottomY);
		if(true){
		if (mRadius != 0) {
			for (int px = (int)(x + (isRighyX ?1: 0) - mRadius);px < x + mRadius;px++) {
				if(px < 0) {px = -1; continue;}
				if(px >= mLayer.width()) break;
				for (int py = (int)(y + (isBottomY ?1: 0) - mRadius);py < y + mRadius;py++) {
					if(py < 0){ py = -1; continue;}
					if(py >= mLayer.height()){break;}
					mLayer.pixels[px][py] = mColor;
				}
			}
		} else {
			mLayer.pixels[x][y] = mColor;
		}
		}else{
			for (int px = (int)(x /*+ (isRighyX ?1: 0) */- mRadius);px < x + mRadius;px++) {
				int ystart = -(int)Math.sqrt(mRadius*mRadius-(px-x)*(px-x));
				int yend = -ystart+y;
				ystart += y;
				
				//Log.i("AAA", px+" "+ystart+" "+yend);
				
				if(px < 0) {px = -1; continue;}
				if(px >= mLayer.width()) break;
				
				for(int py=ystart;py<=yend;py++){
					if(py < 0){ py = -1; continue;}
					if(py >= mLayer.height()){break;}
					mLayer.pixels[px][py] = mColor;
					
				}
				
			}
		}
		return mLayer;
	}

	@Override
	public Layer finish() {
		return mLayer;
	}

	@Override
	public String getName() {
		return "Pencil";
	}

	@Override
	public int getDrawableResource() {
		return R.drawable.ic_create_black_36dp;
	}

	@Override
	public View getOptionsView(LayoutInflater inflater, ViewGroup container) {
		if (optionsView == null) {
			optionsView = inflater.inflate(R.layout.pencil_options, container, false);
			View colorV = optionsView.findViewById(R.id.color);
			colorV.setClickable(true);
			colorV.setOnClickListener(mColorOnClickListener);
			SeekBar thicknessBar = (SeekBar)optionsView.findViewById(R.id.thickness).findViewById(R.id.color_seekbar);
			thicknessBar.setMax((int)Math.sqrt(mLayer.width()*mLayer.height())/10);
			thicknessBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
					@Override
					public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
						((TextView)((ViewGroup)seekBar.getParent()).findViewById(R.id.color_text)).setText(String.valueOf(progress));
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {

					}

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						mRadius = seekBar.getProgress();
					}
				});
		
			
		}

		return optionsView;
	}

	private OnClickListener mColorOnClickListener = new OnClickListener(){

		@Override
		public void onClick(View p1) {
			new ColorPickerDialogBuilder(p1.getContext())
				.setPositiveButton("Ok", new ColorPickerDialogBuilder.OnColorSelectedListener() {
					@Override
					public void onColorSelected(int a, int r, int g, int b) {
						setColor(a, r, g, b);
						optionsView.findViewById(R.id.color).setBackgroundColor(Color.argb(a, r, g, b));
					}
				})
				.setCurrentColor(mColor)
				.show();
		}
	};


}
