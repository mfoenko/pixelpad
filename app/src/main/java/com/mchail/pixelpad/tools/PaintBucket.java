package com.mchail.pixelpad.tools;
import android.graphics.*;
import android.util.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import com.mchail.pixelpad.*;
import com.mchail.pixelpad.views.*;

public class PaintBucket extends Tool
{


	int mColor = 0xFF000000;
	int mTolerance = 500;

	
	View optionsView;
	
	@Override
	public Layer draw(int x, int y, boolean isRightX, boolean isBottomY) {
		fill(x,y, mLayer.pixels[x][y]);
		return mLayer;
	}

	public void fill(int x, int y, int initColor){
		if(x<0 || x >= mLayer.width() || y< 0 || y >= mLayer.height()) return;
		
		int currColor = mLayer.pixels[y][x];
		if(currColor == mColor) return;
		if(Math.abs(ColorUtils.diff(currColor, initColor)) > mTolerance) return;
		Log.i("AAA",""+ ColorUtils.diff(currColor, initColor) );
		mLayer.pixels[y][x] = mColor;
		//System.out.print(x+" "+y+":");
		
		fill(x+1, y, currColor);
		fill(x-1, y, currColor);
		fill(x, y+1, currColor);
		fill(x, y-1, currColor);
		
	}

	@Override
	public Layer finish() {
		return mLayer;
	}


	
	
	@Override
	public String getName() {
		return "Fill";
	}

	@Override
	public int getDrawableResource() {
		return R.drawable.ic_format_color_fill_black_36dp;
	}
	
	
	
	public void setColor(int argb) {
		mColor = argb;
	}
	public void setColor(int a, int r, int g, int b) {
		mColor = Color.argb(a, r, g, b);
	}
	
	@Override
	public View getOptionsView(LayoutInflater inflater, ViewGroup container) {
		if (optionsView == null) {
			optionsView = inflater.inflate(R.layout.pencil_options, container, false);
			View colorV = optionsView.findViewById(R.id.color);
			colorV.setClickable(true);
			colorV.setOnClickListener(mColorOnClickListener);
			SeekBar thicknessBar = (SeekBar)optionsView.findViewById(R.id.thickness).findViewById(R.id.color_seekbar);
			thicknessBar.setMax(757);
			thicknessBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
					@Override
					public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
						((TextView)((ViewGroup)seekBar.getParent()).findViewById(R.id.color_text)).setText(String.valueOf(progress));
					}

					@Override
					public void onStartTrackingTouch(SeekBar seekBar) {

					}

					@Override
					public void onStopTrackingTouch(SeekBar seekBar) {
						mTolerance = seekBar.getProgress();
					}
				});


		}

		return optionsView;
	}

	private OnClickListener mColorOnClickListener = new OnClickListener(){

		@Override
		public void onClick(View p1) {
			new ColorPickerDialogBuilder(p1.getContext())
				.setPositiveButton("Ok", new ColorPickerDialogBuilder.OnColorSelectedListener() {
					@Override
					public void onColorSelected(int a, int r, int g, int b) {
						setColor(a, r, g, b);
						optionsView.findViewById(R.id.color).setBackgroundColor(Color.argb(a, r, g, b));
					}
				})
				.setCurrentColor(mColor)
				.show();
		}
	};
	
	
	
}
