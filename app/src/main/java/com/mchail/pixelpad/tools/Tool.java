package com.mchail.pixelpad.tools;



import android.view.*;
import com.mchail.pixelpad.*;

public abstract class Tool  
{
	
	Layer mLayer;

	public boolean begin(Layer toolLayer) {
		mLayer = toolLayer;
		return true;
	}
	public abstract Layer draw(int x, int y, boolean isRightX, boolean isBottomY);
	public Layer finish(){
		return mLayer;
	}
	
	public abstract String getName();
	public abstract int getDrawableResource();
	
	
	public View getOptionsView(LayoutInflater inflater, ViewGroup containee){
		return null;
	}
}
