package com.mchail.pixelpad.tools;

import android.graphics.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import com.mchail.pixelpad.*;
import com.mchail.pixelpad.views.*;

public class Brush extends Pencil 
{

	
	@Override
	public Layer draw(int x, int y, boolean isRighyX, boolean isBottomY) {
		//Log.i("AAA", x + " " + y + " " + isRighyX + " " + isBottomY);
		if(false){
			if (mRadius != 0) {
				for (int px = (int)(x + (isRighyX ?1: 0) - mRadius);px < x + mRadius;px++) {
					if(px < 0) {px = -1; continue;}
					if(px >= mLayer.width()) break;
					for (int py = (int)(y + (isBottomY ?1: 0) - mRadius);py < y + mRadius;py++) {
						if(py < 0){ py = -1; continue;}
						if(py >= mLayer.height()){break;}
						mLayer.pixels[px][py] = mColor;
					}
				}
			} else {
				mLayer.pixels[x][y] = mColor;
			}
		}else{
			for (int px = (int)(x /*+ (isRighyX ?1: 0) */- mRadius);px < x + mRadius;px++) {
				int ystart = -(int)Math.sqrt(mRadius*mRadius-(px-x)*(px-x));
				int yend = -ystart+y;
				ystart += y;

				//Log.i("AAA", px+" "+ystart+" "+yend);

				if(px < 0) {px = -1; continue;}
				if(px >= mLayer.width()) break;

				
				for(int py=ystart;py<=yend;py++){
					if(py < 0){ py = -1; continue;}
					if(py >= mLayer.height()){break;}
					final int c = mLayer.pixels[px][py];
					mLayer.pixels[px][py] = ColorUtils.add(c,mColor);

				}

			}
		}
		return mLayer;
	}

	@Override
	public String getName() {
		return "Brush";
	}


	
	
	@Override
	public int getDrawableResource() {
		return R.drawable.ic_brush_black_36dp;
	}
	

}
