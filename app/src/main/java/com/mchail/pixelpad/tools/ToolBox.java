package com.mchail.pixelpad.tools;

public class ToolBox
{
	
	public final Tool[] tools;
	public final String name;
	public final int icon;

	public ToolBox(String name, int icon, Tool... tools) {
		this.tools = tools;
		this.name = name;
		this.icon = icon;
	}
	
	
	
	
}
