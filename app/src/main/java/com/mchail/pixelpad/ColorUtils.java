package com.mchail.pixelpad;

import android.graphics.*;

public class ColorUtils 
{
	
	private ColorUtils(){
		
	}
	
	public static int add(int argb1, int argb2){
		int a,r,g,b;
		float a1= Color.alpha(argb1)/255f;
		float a2= Color.alpha(argb2)/255f;
		a = clamp(Color.alpha(argb1)+Color.alpha(argb2));
		r =clamp((int)(a1*Color.red(argb1)+a2*Color.red(argb2)));
		g =clamp((int)(a1*Color.green(argb1)+a2*Color.green(argb2)));
		b =clamp((int)(a1*Color.blue(argb1)+a2*Color.blue(argb2)));
		
		
		return Color.argb(a,r,g,b);
	}
	
	public static int sub(int argb1, int argb2){
		int a,r,g,b;
		float a1= Color.alpha(argb1)/255f;
		float a2= Color.alpha(argb2)/255f;
		a = clamp(Color.alpha(argb1)+Color.alpha(argb2));
		r =clamp((int)(a1*(255-Color.red(argb1))+a2*(255-Color.red(argb2))));
		g =clamp((int)(a1*(255-Color.green(argb1))+a2*(255-Color.green(argb2))));
		b =clamp((int)(a1*(255-Color.blue(argb1))+a2*(255-Color.blue(argb2))));
		

		return Color.argb(a,r,g,b);
	}
	
	public static int diff(int argb1, int argb2){
		
		return Color.red(argb1)-Color.red(argb2)+
		Color.green(argb1)-Color.green(argb2)+
		Color.blue(argb1)-Color.blue(argb2);
		
	}
	
	public static int clamp(int c){
		if(c>255)
			return 255;
		if(c<0)
			return 0;
			
			return c;
	}
	
}
