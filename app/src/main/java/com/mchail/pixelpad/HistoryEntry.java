package com.mchail.pixelpad;

import android.util.*;
import java.util.*;

public class HistoryEntry 
{
	public Layer layer;
	public int[][] pixels;

	public HistoryEntry(){}
	
	public HistoryEntry(Layer layer) {
		setLayer(layer);
	}

	public void setLayer(Layer layer) {
		this.layer = layer;
		this.pixels = copy(layer.pixels);
	}

	public void recall(){
		Log.i("AAA", Arrays.toString(layer.pixels[0]) + "=" + Arrays.toString(pixels[0]));
		layer.pixels = pixels;
	}
	
	private static int[][] copy(int[][]og){
		final int l=og.length;
		final int w=og[0].length;
		int[][] n = new int[l][w];
		for(int i=0;i<l;i++){
			for(int j=0;j<w;j++){
				n[i][j] = og[i][j];
			}
		}
		return n;
	}
	
}
