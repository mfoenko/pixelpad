package com.mchail.pixelpad.views;

import android.app.*;
import android.content.*;
import android.graphics.*;
import android.graphics.drawable.*;
import android.view.*;
import android.widget.*;
import android.widget.AdapterView.*;
import com.mchail.pixelpad.tools.*;
import android.util.*;

public class ToolBoxView extends Spinner implements OnItemSelectedListener{

	private ToolBox mToolBox;
	

	private boolean isOpen;

	//private Paint debugPaint;

	private OnToolSelectListener mListener;
	private ToolBoxAdapter mToolBoxAdapter;
	
	public ToolBoxView(Context c) {
		super(c);
		init();
	}

	public ToolBoxView(Context c, AttributeSet set) {
		super(c, set);
		init();
	}

	
	
	public void init() {
		setAdapter(mToolBoxAdapter = new ToolBoxAdapter());
		setOnItemSelectedListener(this);

		//debugPaint = new Paint();
		//debugPaint.setColor(0xFF00CC00);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {


		return super.onTouchEvent(event);
	}

	public void setToolBox(ToolBox mToolBox) {
		this.mToolBox = mToolBox;
		mToolBoxAdapter.setTools(mToolBox.tools);
		invalidate();
	}
	
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if(mToolBox == null) return;
		//Drawable icon = getResources().getDrawable(mToolBox.icon);
		//icon.setBounds(0, 0, getHeight(), getWidth());
		//icon.draw(canvas);

	}
	
	public void setOnToolSelectListener(OnToolSelectListener listener){
		mListener = listener;
	}

	private static class ToolBoxAdapter extends BaseAdapter{
		public Tool[] mTools;

		public ToolBoxAdapter(){
			
		}
		
		public ToolBoxAdapter(Tool[] mTools) {
			this.mTools = mTools;
		}

		public void setTools(Tool[] mTools) {
			this.mTools = mTools;
			notifyDataSetChanged();
		}


		@Override
		public int getCount() {
			if(mTools==null) return 0;
			return mTools.length;
		}

		@Override
		public Object getItem(int p1) {
			return mTools[p1];
		}

		@Override
		public long getItemId(int p1) {
			return 0;
		}

		@Override
		public View getView(int p1, View p2, ViewGroup p3) {
			ImageView iv = (ImageView)p2;
			if(iv == null){
				iv = new ImageView(p3.getContext());
			}
			iv.setImageResource(mTools[p1].getDrawableResource());
			return iv;
		}

		
		
	}
	@Override
	public void onItemSelected(AdapterView<?> p1, View p2, int p3, long p4) {
		Log.i("AAA", "TOOOOOOL");
		if(mListener != null)
			mListener.OnToolSelect(mToolBox.tools[p3]);
		Log.i("AAA", "TOOLLLLLLL");
		
		isOpen = false;
		invalidate();
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {

	}

	public static interface OnToolSelectListener{
		
		public void OnToolSelect(Tool tool);
		
	}


}
