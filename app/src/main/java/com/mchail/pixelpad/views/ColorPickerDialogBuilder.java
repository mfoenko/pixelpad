package com.mchail.pixelpad.views;
import android.app.*;
import android.content.*;
import android.graphics.*;
import android.view.*;
import android.widget.*;
import com.mchail.pixelpad.*;

public class ColorPickerDialogBuilder extends AlertDialog.Builder
{

	public static final int SEEKER_ALPHA = 0;
	public static final int SEEKER_RED = 1;
	public static final int SEEKER_GREEN = 2;
	public static final int SEEKER_BLUE = 3;
	public static final int NUM_COLOR_VALS = 4;
	private View mDialogView;


	public ColorPickerDialogBuilder(Context c){
		super(c);
		setTitle("Color");
		setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface p1, int p2) {
				p1.cancel();
			}
		});



		LayoutInflater inflater = LayoutInflater.from(c);
		final View mDialogView = (ViewGroup)inflater.inflate(R.layout.color_picker_dialog, null, false);
		ViewGroup pickerContainer = (ViewGroup) mDialogView.findViewById(R.id.color_seekers);

		for(int i = 0;i < NUM_COLOR_VALS; i++){

			((SeekBar)pickerContainer.getChildAt(i).findViewById(R.id.color_seekbar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					((TextView)((ViewGroup)seekBar.getParent()).findViewById(R.id.color_text)).setText(String.valueOf(progress));
				}

				@Override
				public void onStartTrackingTouch(SeekBar seekBar) {

				}

				@Override
				public void onStopTrackingTouch(SeekBar seekBar) {

				}
			});
		}

		this.mDialogView = mDialogView;



			setView(mDialogView);
		
	}

	public ColorPickerDialogBuilder setPositiveButton(CharSequence text, final OnColorSelectedListener listener) {
		super.setPositiveButton(text, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				int[] color = new int[NUM_COLOR_VALS];
				ViewGroup pickerContainer = (ViewGroup) mDialogView.findViewById(R.id.color_seekers);
				for (int i = 0; i < NUM_COLOR_VALS; i++) {
					color[i] = ((SeekBar) pickerContainer.getChildAt(i).findViewById(R.id.color_seekbar)).getProgress();
				}


				listener.onColorSelected(color[SEEKER_ALPHA], color[SEEKER_RED], color[SEEKER_GREEN], color[SEEKER_BLUE]);

			}
		});

		return this;
	}
	
	public ColorPickerDialogBuilder setCurrentColor(int a,int r,int g,int b){
		int[] colors = new int[]{a,r,g,b};
		ViewGroup pickerContainer = (ViewGroup) mDialogView.findViewById(R.id.color_seekers);
		for(int i = 0;i < NUM_COLOR_VALS; i++)
			((SeekBar)pickerContainer.getChildAt(i).findViewById(R.id.color_seekbar)).setProgress(colors[i]);
					
		return this;
	}
	
	public ColorPickerDialogBuilder setCurrentColor(int argb){
		
		return setCurrentColor(Color.alpha(argb), Color.red(argb), Color.green(argb), Color.blue(argb));
		
	}
	

	public static interface OnColorSelectedListener{
		public void onColorSelected(int a, int r, int g, int b);
	}
}
