package com.mchail.pixelpad.views;

import android.content.*;
import android.content.res.*;
import android.graphics.*;
import android.util.*;
import android.view.*;
import com.mchail.pixelpad.*;
import com.mchail.pixelpad.tools.*;
import java.util.*;
import java.nio.*;

public class DrawPadView extends View {
	Point mDrawAreaOffset = new Point();
	Rect mDrawAreaOffsetBounds = new Rect();
	static final int MIN_CELLS_DRAWN = 4;

	float mCellSize = 16;
	float MAX_CELL_SIZE = 64;
	float MIN_CELL_SIZE = 1;

	Paint mGridPaint;
	Paint mPixelPaint;
	Rect mPixel;

	PixelPad mPixelPad;


	public DrawPadView(Context c) {
		super(c);
		init();
	}

	public DrawPadView(Context c, AttributeSet set) {
		super(c, set);
		init();
	}

	public void setPixelPad(PixelPad mPixelPad) {
		this.mPixelPad = mPixelPad;

		mDrawAreaOffsetBounds.set((int)(-mPixelPad.canvasSize.x * mCellSize + 1), (int)(-mPixelPad.canvasSize.y * mCellSize + 1), (int)(mPixelPad.canvasSize.x * mCellSize + 1), (int)(mPixelPad.canvasSize.y * mCellSize + 1));
	}


	private void init() {

		mGridPaint = new Paint();
		mGridPaint.setColor(0xFF000000);
		mGridPaint.setTextSize(25);

		mPixelPaint = new Paint();
		mPixel = new Rect();

		mCellSize *= 
			Resources.getSystem().getDisplayMetrics().density;
		MAX_CELL_SIZE *= 
			Resources.getSystem().getDisplayMetrics().density;
		MIN_CELL_SIZE *= 
			Resources.getSystem().getDisplayMetrics().density;


		mScaleListener = new ScaleGestureDetector(getContext(), new ScaleListener());
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (mPixelPad == null) return;

		int startX = (int)(-mDrawAreaOffset.x / mCellSize);
		if (startX < 0) {
			startX = 0;
		}
		int endX = startX + (int)(getWidth() / mCellSize) + 2;
		if (endX > mPixelPad.canvasSize.x) {
			endX = mPixelPad.canvasSize.x;
		}

		int startY = (int)(-mDrawAreaOffset.y / mCellSize);
		if (startY < 0) {
			startY = 0;
		}
		int endY = startY + (int)(getHeight() / mCellSize) + 2;
		if (endY > mPixelPad.canvasSize.y) {
			endY = mPixelPad.canvasSize.y;
		}

//		float left = mDrawAreaOffset.x%mCellSize+mCellSize;
//		float top = mDrawAreaOffset.y%mCellSize+mCellSize;
		float left = mDrawAreaOffset.x < 0 ?mDrawAreaOffset.x % mCellSize: mDrawAreaOffset.x;
		float top = mDrawAreaOffset.y < 0 ?mDrawAreaOffset.y % mCellSize: mDrawAreaOffset.y;
		float right = left + (endX - startX) * mCellSize;
		float bottom = top + (endY - startY) * mCellSize;

		for (int l = startX;l <= endX;l++) {
			float x = left + mCellSize * (l - startX);

			canvas.drawLine(x, top, x, bottom, mGridPaint);
			canvas.drawText("" + l, x, 500, mGridPaint);
		}

		for (int l = startY;l <= endY;l++) {
			float y = top + mCellSize * (l - startY);
			canvas.drawLine(right, y, left, y, mGridPaint);

			canvas.drawText("" + l, 10, y , mGridPaint);

		}


		for (int c = startX;c < endX;c++) {
			float x = left + mCellSize * (c - startX);

			mPixel.left = (int)x;
			mPixel.right = (int)(mCellSize + x);
			for (int r = startY;r < endY;r++) {
				float y = top + mCellSize * (r - startY);
				mPixel.top = (int)y;
				mPixel.bottom = (int)(y + mCellSize);
				//mPixelPaint.setColor(0);

				boolean isColored = false;
				for (Layer l:mPixelPad.layers) {
					if (l.pixels[c][r] != 0) {
						mPixelPaint.setColor(l.pixels[c][r]);
						isColored = true;
						break;
					}
				}

				if (isColored)
					canvas.drawRect(mPixel, mPixelPaint);

			}
		}





	}





	MotionEvent.PointerCoords oldCoords = new MotionEvent.PointerCoords();
	ScaleGestureDetector mScaleListener;

	long timeOfGesture;
	private static final long GESTURE_COOLDOWN = 250;

	@Override
	public boolean onTouchEvent(final MotionEvent event) {
		if (event.getPointerCount() > 1) {
			timeOfGesture = System.currentTimeMillis();
			mScaleListener.onTouchEvent(event);
			switch (event.getAction()) {
				case MotionEvent.ACTION_POINTER_2_DOWN:
					event.getPointerCoords(1, oldCoords);
					return true;
				case MotionEvent.ACTION_MOVE:
					MotionEvent.PointerCoords newCoords = new MotionEvent.PointerCoords();
					event.getPointerCoords(1, newCoords);

					mDrawAreaOffset.x += newCoords.x - oldCoords.x;
					mDrawAreaOffset.y += newCoords.y - oldCoords.y;
					correctBounds();
					oldCoords = newCoords;

					invalidate();
					return true;
			}
		} else

		if (mPixelPad.tool != null && System.currentTimeMillis() - timeOfGesture > GESTURE_COOLDOWN) {
			switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					float x1 = ((event.getX() - mDrawAreaOffset.x) / mCellSize);
					float y1 = ((event.getY() - mDrawAreaOffset.y) / mCellSize);

					if (x1 < mPixelPad.canvasSize.x && x1 >= 0 && y1 < mPixelPad.canvasSize.y && y1 >= 0) {
						mPixelPad.addToHistory();
					}
					break;
				case MotionEvent.ACTION_MOVE:


					float x = ((event.getX() - mDrawAreaOffset.x) / mCellSize);
					float y = ((event.getY() - mDrawAreaOffset.y) / mCellSize);



					if (x < mPixelPad.canvasSize.x && x >= 0 && y < mPixelPad.canvasSize.y && y >= 0) {

						mPixelPad.tool.draw((int)x, (int)y, (x - (int)x) >= .5f, (y - (int)y) >= .5f);
						postInvalidate();
					}
					break;

			}
		}
		if (event.getAction() == MotionEvent.ACTION_DOWN) return true;

		return super.onTouchEvent(event);
	}

	private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

		@Override
		public boolean onScale(ScaleGestureDetector detector) {

			mCellSize *= detector.getScaleFactor();
			mDrawAreaOffsetBounds.set((int)(-mPixelPad.canvasSize.x * mCellSize + 1), (int)(-mPixelPad.canvasSize.y * mCellSize + 1), (int)(mPixelPad.canvasSize.x * mCellSize + 1), (int)(mPixelPad.canvasSize.y * mCellSize + 1));
			correctBounds();
			return true;
		}

	}

	private Point getCoordinates(float x, float y) {
		Point p = new Point();
		p.x = (int)((x - mDrawAreaOffset.x) / mCellSize);
		p.y = (int)((y - mDrawAreaOffset.y) / mCellSize);
		return p;
	}

	private void correctBounds() {

		if (mCellSize < MIN_CELL_SIZE) {
			mCellSize = MIN_CELL_SIZE;
		}
		if (mCellSize > MAX_CELL_SIZE) {
			mCellSize = MAX_CELL_SIZE;
		}

		if (!mDrawAreaOffsetBounds.contains(mDrawAreaOffset.x, mDrawAreaOffset.y)) {
			if (mDrawAreaOffset.x < mDrawAreaOffsetBounds.left) {
				mDrawAreaOffset.x = mDrawAreaOffsetBounds.left;
			}
			if (mDrawAreaOffset.x > mDrawAreaOffsetBounds.right) {
				mDrawAreaOffset.x = mDrawAreaOffsetBounds.right;
			}
			if (mDrawAreaOffset.y < mDrawAreaOffsetBounds.top) {
				mDrawAreaOffset.y = mDrawAreaOffsetBounds.top;
			}
			if (mDrawAreaOffset.y > mDrawAreaOffsetBounds.bottom) {
				mDrawAreaOffset.y = mDrawAreaOffsetBounds.bottom;
			}
		}
	}



}
