package com.mchail.pixelpad;

import android.graphics.*;

public class Layer
{
	
	public int[][] pixels;
	
	public Layer(Layer old){
		this(old.pixels);
	}

	public Layer(int[][] pixels) {
		this.pixels = pixels;
	}
	
	public Layer(int w, int h) {
		this.pixels = new int[w][h];
	}
	public Layer(Point size) {
		this.pixels = new int[size.x][size.y];
	}
	
	
	public void merge(Layer other, int xOff, int yOff, boolean overWrite){
		for(int x=0;x<other.width();x++){
			for(int y=0;y<other.height();y++){
				if(x+xOff < 0 || x+xOff >= this.width()){
					y=other.height();
					continue;
				}			
				if(y+yOff < 0 || y+yOff >= this.height()){
					y=other.height();
					continue;
				}
				if(overWrite || this.pixels[x+xOff][y+yOff] == 0){
					this.pixels[x+xOff][y+yOff] = other.pixels[x][y];
				}
			}
		}
	}
	
	public int width(){
		return pixels.length;
	}
	
	public int height(){
		return pixels[0].length;
	}
	
	
	public static Layer merge(Layer... layers){
		if(layers.length == 0) return null;
		Layer merged = new Layer(layers[0]);
		for(int i=1;i<layers.length;i++){
			merged.merge(layers[i],0,0,false);
		}
		return merged;
	}

	public Bitmap getBitmap(){
		final int height = height();
		final int width = width();
		int[] buffer = new int[height*width];
		for(int x=0;x<width;x++){
			for(int y=0;y<height;y++){
				buffer[x*width+y] = pixels[y][x];
			}
		}
		return Bitmap.createBitmap(buffer, width, height, Bitmap.Config.ARGB_8888);
	}
}
