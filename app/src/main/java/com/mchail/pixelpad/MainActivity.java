package com.mchail.pixelpad;


import android.app.*;
import android.content.*;
import android.graphics.*;
import android.os.*;
import android.support.v4.widget.*;
import android.support.v7.app.*;
import android.support.v7.widget.*;
import android.view.*;
import android.widget.*;
import com.mchail.pixelpad.tools.*;
import com.mchail.pixelpad.views.*;
import java.io.*;

public class MainActivity extends Activity 
{
	private PixelPad mPixelPad;
	
	private DrawerLayout mDrawerLayout;
	private DrawPadView mDrawPad;
	private ScrollView mOptionsView;
	private RecyclerView mLayersDrawer;

	private LayerDrawerAdapter mLayerAdapter;
	private ActionBarDrawerToggle mDrawerToggle;
	
	private MenuItem[] mToolBoxMenus;

	public static final int DEFAULT_IMAGE_HEIGHT = 64;
	public static final int DEFAULT_IMAGE_WIDTH = 64;

	public static final String KEY_IMAGE_HEIGHT = "height";
	public static final String KEY_IMAGE_WIDTH = "width";

	public ToolBox[] mToolBoxes = new ToolBox[]{
		new ToolBox("Drawing", R.drawable.ic_create_black_36dp, new Pencil(), new Brush(), new PaintBucket())
	};

	public ToolBoxView.OnToolSelectListener mToolListener = new ToolBoxView.OnToolSelectListener(){

		@Override
		public void OnToolSelect(Tool tool) {
			selectTool(tool);
		}

	
	};

	private boolean isDrawerOpen = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

		
		Intent recievedIntent = getIntent();
		int height,width;
		if(recievedIntent != null){
			height = recievedIntent.getIntExtra(KEY_IMAGE_HEIGHT, DEFAULT_IMAGE_HEIGHT);
			width = recievedIntent.getIntExtra(KEY_IMAGE_WIDTH, DEFAULT_IMAGE_WIDTH);
		}else{
			height = DEFAULT_IMAGE_HEIGHT;
			width = DEFAULT_IMAGE_WIDTH;
		}

		mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);



		mDrawPad = (DrawPadView)findViewById(R.id.pad);
		mDrawPad.setPixelPad(mPixelPad = new PixelPad(width, height));
		
		mOptionsView = (ScrollView)findViewById(R.id.tool_settings);

		mLayersDrawer = (RecyclerView)findViewById(R.id.layers_list);
		mLayersDrawer.setLayoutManager(new LinearLayoutManager(this));
		mLayersDrawer.setAdapter(mLayerAdapter = new LayerDrawerAdapter(mPixelPad.layers));

		mLayerAdapter.setOnLayerSelectListener(new LayerDrawerAdapter.OnLayerSelectListener() {
			@Override
			public void onLayerSelect(int layer) {
				mPixelPad.curentLayerIndex = layer;
			}
		});


		mDrawerToggle = new ActionBarDrawerToggle(this ,mDrawerLayout, R.string.drawer_open, R.string.drawer_closed){
			@Override
			public void onDrawerStateChanged(int newState) {
				super.onDrawerStateChanged(newState);
				mLayerAdapter.notifyDataSetChanged();
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				isDrawerOpen = true;
				invalidateOptionsMenu();
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				isDrawerOpen = false;
				invalidateOptionsMenu();
			}
		};

		mDrawerLayout.setDrawerListener(mDrawerToggle);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		
		mPixelPad.addToHistory();
    }

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	protected void onPause() {
		super.onPause();
		File outFile = new File("/sdcard/img.png");
		try {
			outFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
			
		}
		try {
			FileOutputStream out = new FileOutputStream(outFile);
			mPixelPad.getImage().compress(Bitmap.CompressFormat.PNG, 100, out);
		} catch (FileNotFoundException e) {e.printStackTrace();}
		
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		final int l = mToolBoxes.length;
		mToolBoxMenus = new MenuItem[l];
		for(int i=0;i<l;i++){
			ToolBox tBox = mToolBoxes[i];
			inflater.inflate(R.menu.tool_box, menu);
			MenuItem mItem = menu.getItem(menu.size()-1);
			ToolBoxView tbv = (ToolBoxView)mItem.getActionView();
			tbv.setToolBox(tBox);
			tbv.setOnToolSelectListener(mToolListener);
			mToolBoxMenus[i] = mItem;
		}
		inflater.inflate(R.menu.add_layer, menu);
		inflater.inflate(R.menu.undo, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		for(MenuItem mItem:mToolBoxMenus){
			mItem.setVisible(!isDrawerOpen);
		}
		menu.findItem(R.id.undo).setVisible(!isDrawerOpen);

		menu.findItem(R.id.add_layer).setVisible(isDrawerOpen);

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(mDrawerToggle.onOptionsItemSelected(item)){
			return true;
		}

		switch(item.getItemId()){

			case R.id.add_layer:
				mPixelPad.addLayer();
				mLayerAdapter.notifyDataSetChanged();
				return true;
			case R.id.undo:
				mPixelPad.recallHistory();
				mDrawPad.invalidate();
				mLayerAdapter.notifyDataSetChanged();
				return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	

	public void selectTool(Tool tool){
		
		if(tool == null || mPixelPad == null) return;

		mPixelPad.finish();
		mPixelPad.setTool(tool);
		mPixelPad.begin();

		mOptionsView.removeAllViews();
		mOptionsView.addView(tool.getOptionsView(getLayoutInflater(), mOptionsView));
		
	}
	
	
}

