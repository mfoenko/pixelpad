package com.mchail.pixelpad;
import android.app.*;
import android.content.*;
import android.graphics.*;
import android.os.*;
import android.support.v7.app.*;
import android.support.v7.widget.*;
import android.view.*;
import android.view.View.*;
import android.widget.*;
import java.util.*;

import android.app.AlertDialog;

public class HomeActivity extends Activity
{
	
	
	private ArrayList<HomeMenuItem> menu = new ArrayList<HomeMenuItem>();
	private RecyclerView mRecyclerView;
	private HomeMenuItemAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);
		
		mRecyclerView = (RecyclerView)findViewById(R.id.recycler_view);
		GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
		mRecyclerView.setLayoutManager(layoutManager);
		
		menu.add(new HomeMenuItem("New Project", R.drawable.ic_plus_black_36dp, 0xFF00DD88){
				@Override
				public void onClick(final Context context) {
					final View dialogContent = LayoutInflater.from(context).inflate(R.layout.new_project_dialog, null, false);
					
					new AlertDialog.Builder(context).setTitle("New Project").
					setView(dialogContent)
					.setPositiveButton("Create", new DialogInterface.OnClickListener(){

							@Override
							public void onClick(DialogInterface p1, int p2) {
								int height = Integer.parseInt(((TextView)dialogContent.findViewById(R.id.height)).getText().toString());
								int width = Integer.parseInt(((TextView)dialogContent.findViewById(R.id.width)).getText().toString());
								
								Intent intent = new Intent(context, MainActivity.class);
								intent.putExtra(MainActivity.KEY_IMAGE_HEIGHT, height);
								intent.putExtra(MainActivity.KEY_IMAGE_WIDTH, width);
								startActivity(intent);
							}
							
						
					}).setNegativeButton("Cancel", null).show();
					
				}
			});
		menu.add(new HomeMenuItem("Open", R.drawable.ic_folder_black_36dp, 0xFF00DD88){
				@Override
				public void onClick(Context context) {

				}
			});
		mAdapter = new HomeMenuItemAdapter(menu);
		mRecyclerView.setAdapter(mAdapter);
		
	}
	
	
	private static abstract class HomeMenuItem{
		
		public boolean usesBitmapIcon;

		public String name;
		public Bitmap iconBitmap;
		public int iconResource;
		public int bgColor;

		public HomeMenuItem(String name, Bitmap iconBitmap, int bgColor) {
			this.name = name;
			this.usesBitmapIcon = true;
			this.iconBitmap = iconBitmap;
			this.bgColor = bgColor;
		}
		public HomeMenuItem(String name, int iconResource, int bgColor) {
			this.name = name;
			this.usesBitmapIcon = false;
			this.iconResource = iconResource;
			this.bgColor = bgColor;
		}
		
		public abstract void onClick(Context context);
	}
	
	private static final class HomeMenuItemAdapter extends RecyclerView.Adapter<HomeMenuItemViewHolder> {
		
		private ArrayList<HomeMenuItem> menu;

		public HomeMenuItemAdapter(ArrayList<HomeMenuItem> menu) {
			this.menu = menu;
		}
		

		@Override
		public HomeActivity.HomeMenuItemViewHolder onCreateViewHolder(ViewGroup p1, int p2) {
			View v = LayoutInflater.from(p1.getContext()).inflate(R.layout.home_menu_item, p1, false);
			return new HomeMenuItemViewHolder(v);
		}

		@Override
		public void onBindViewHolder(HomeActivity.HomeMenuItemViewHolder holder, int pos) {
			final HomeMenuItem item = menu.get(pos);
			holder.nameTV.setText(item.name);
			if(item.usesBitmapIcon){
				holder.iconIV.setImageBitmap(item.iconBitmap);
			}else{
				holder.iconIV.setImageResource(item.iconResource);
			}
			holder.itemView.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View p1) {
						if(Build.VERSION.SDK_INT == Build.VERSION_CODES.LOLLIPOP){
							//ObjectAnimator.ofFloat(p1, "z", new float[]{0,30,0}).setDuration(1000).start();
						}
						item.onClick(p1.getContext());
						
					}
					
			});
			holder.itemView.setBackgroundColor(item.bgColor);
		}

		@Override
		public int getItemCount() {
			return menu.size();
		}
		
		
		
	}
	
	protected static final class HomeMenuItemViewHolder extends RecyclerView.ViewHolder{
		
		public ImageView iconIV;
		public TextView nameTV;
		
		public HomeMenuItemViewHolder(View itemView){
			super(itemView);
			iconIV = (ImageView)itemView.findViewById(R.id.icon);
			nameTV = (TextView)itemView.findViewById(R.id.name);
			
		}
	}
	
}
